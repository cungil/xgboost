(in-package :xgboost)

(defmacro with-output-value ((name type) &body body)
  `(cffi:with-foreign-object (,name ,type)
     ,@body
     (cffi:mem-ref ,name ,type)))

(defmacro with-output-values (bindings &body body)
  `(cffi:with-foreign-objects
       ,(loop for (name type) in bindings collect (list name type))
     ,@body
     (list ,@(loop for (name type) in bindings
		collect `(cffi:mem-ref ,name ,type)))))

;; FIXME: returned objects may need to be freed ?
(defmacro with-output-array ((name type count) &body body)
  `(cffi:with-foreign-objects ((,name :pointer) (,count :int))
     ,@body
     (loop for i below (cffi:mem-ref ,count :int)
	collect (cffi:mem-aref (cffi:mem-ref ,name :pointer) ,type i))))

(defvar *boosters*
  (tg:make-weak-hash-table :weakness :value))

(defvar *matrices*
  (tg:make-weak-hash-table :weakness :value))

(defun check-errors (x)
  (unless (zerop x)
    (error "~A" (xgb.ffi:xgb-get-last-error))))

(defgeneric save (object file))

(defclass xgb-pointer ()
  ((pointer :initarg :pointer :accessor pointer)))

(defclass xgb-booster (xgb-pointer)
  ((params :initarg :params :accessor params)))

(defclass xgb-matrix (xgb-pointer)
  ())

(defmethod nrow ((dmat xgb-matrix))
  (with-output-value (nrow :int)
    (check-errors (xgb.ffi:xgd-matrix-num-row (pointer dmat) nrow))))

(defmethod ncol ((dmat xgb-matrix))
  (with-output-value (ncol :int)
    (check-errors (xgb.ffi:xgd-matrix-num-col (pointer dmat) ncol))))

(defmethod dims ((dmat xgb-matrix))
  (list (nrow dmat) (ncol dmat)))

(defmethod print-object ((dmat xgb-matrix) stream)
  (print-unreadable-object (dmat stream :type t)
    (format stream "~Ax~A~{ ~A~}" (nrow dmat) (ncol dmat)
	    (loop for field in '(:label :weight :group-ptr :base-margin)
	       when (get-info dmat field) collect field))))

(defun xgb-matrix (ptr)
  (assert (cffi:pointerp ptr))
  (let ((xgb-matrix (make-instance 'xgb-matrix :pointer ptr)))
    (tg:finalize xgb-matrix (lambda () (xgb.ffi:xgd-matrix-free ptr)))
    (setf (gethash (cffi:pointer-address ptr) *matrices*) xgb-matrix)))

(defun xgb-booster (ptr)
  (assert (cffi:pointerp ptr))
  (let ((xgb-booster (make-instance 'xgb-booster :pointer ptr)))
    (tg:finalize xgb-booster (lambda () (xgb.ffi:xg-booster-free ptr)))
    (setf (gethash (cffi:pointer-address ptr) *boosters*) xgb-booster)))

(defun version ()
  (with-output-values ((major :int) (minor :int) (patch :int))
    (xgb.ffi::xg-boost-version major minor patch)))

(defun info ()  
  (with-output-value (out :string)
    (check-errors (xgb.ffi::xg-build-info out))))

(defun get-global-config ()
  (with-output-value (out :string)
    (check-errors (xgb.ffi::xgb-get-global-config out))))

(defun set-global-config (config)
  (check-errors (xgb.ffi::xgb-set-global-config config)))

(defun create-matrix (mat &key (missing most-negative-single-float) (nthreads 1))
  (declare (optimize speed))
  (assert (typep mat 'array))
  (let ((nrow (array-dimension mat 0))
	(ncol (array-dimension mat 1)))
    (xgb-matrix
     (with-output-value (handle :pointer)
       (cffi:with-foreign-object (data :float (* nrow ncol))
	 (loop for i fixnum below (* nrow ncol)
	    do (setf (cffi:mem-aref data :float i) (if (row-major-aref mat i)
						       (coerce (row-major-aref mat i) 'single-float)
						       most-negative-single-float)))
	 (if (= nthreads 1)
	     (check-errors (xgb.ffi:xgd-matrix-create-from-mat data nrow ncol missing handle))
	     (check-errors (xgb.ffi:xgd-matrix-create-from-mat-omp data nrow ncol missing handle nthreads))))))))

;; from CSR / Compressed Sparse Row
;; const size_t* indptr, const unsigned* indices, const float* data, size_t nindptr, size_t nelem, size_t num_col, DMatrixHandle* out
;; http://www.scipy-lectures.org/advanced/scipy_sparse/csr_matrix.html#examples

(defun create-matrix-csr (data column-indices row-starts columns)
  (xgb-matrix
   (with-output-value (handle :pointer)
     (cffi:with-foreign-objects ((dat :float (length data))
				 (cid :unsigned-int (length column-indices))
				 (rst :unsigned-long (length row-starts)))
       (loop for i below (length data)
	  do (setf (cffi:mem-aref dat :float i) (coerce (elt data i) 'single-float)))
       (loop for i below (length column-indices)
	  do (setf (cffi:mem-aref cid :unsigned-int i) (coerce (elt column-indices i) 'integer)))
       (loop for i below (length row-starts)
	  do (setf (cffi:mem-aref rst :unsigned-long i) (coerce (elt row-starts i) 'integer)))
       (check-errors (xgb.ffi:xgd-matrix-create-from-csr-ex
		      rst cid dat (length row-starts) (length data) columns handle))))))

;; (create-matrix-csr '(1 2 3 4 5 6) '(0 2 2 0 1 2) '(0 2 3 6) 3)
;; the first row is (1 0 2)
;; the second row is (0 0 3)
;; the third row is (4 5 6)

;; from CSC / Compressed Sparse Column
;; const size_t* col_ptr, const unsigned* indices, const float* data, size_t nindptr, size_t nelem, size_t num_row, DMatrixHandle* out
;; http://www.scipy-lectures.org/advanced/scipy_sparse/csc_matrix.html#examples

(defun create-matrix-csc (data row-indices column-starts rows)
  (xgb-matrix
   (with-output-value (handle :pointer)
     (cffi:with-foreign-objects ((dat :float (length data))
				 (rid :unsigned-int (length row-indices))
				 (cst :unsigned-long (length column-starts)))
       (loop for i below (length data)
	  do (setf (cffi:mem-aref dat :float i) (coerce (elt data i) 'single-float)))
       (loop for i below (length row-indices)
	  do (setf (cffi:mem-aref rid :unsigned-int i) (coerce (elt row-indices i) 'integer)))
       (loop for i below (length column-starts)
	  do (setf (cffi:mem-aref cst :unsigned-long i) (coerce (elt column-starts i) 'integer)))
       (check-errors (xgb.ffi:xgd-matrix-create-from-csc-ex
		      cst rid dat (length column-starts) (length data) rows handle))))))

;; (create-matrix-csc '(1 4 5 2 3 6) '(0 2 2 0 1 2) '(0 2 3 6) 3)
;; the first column is (1 0 4), the second column in (0 0 5), the third column is (2 3 6)
;; the first row is (1 0 2)
;; the second row is (0 0 3)
;; the third row is (4 5 6)

(defun read-data-file (file &optional verbose)
  ;; silent flag is ignored ?
  (xgb-matrix
   (with-output-value (handle :pointer)
     (check-errors (xgb.ffi:xgd-matrix-create-from-file (namestring file) (if verbose 0 1) handle)))))

(defun read-features (file)
  (with-open-file (in file)
    (loop for line = (read-line in nil nil)
       while line
       collect (destructuring-bind (id name type)
		   (loop for i = 0 then (1+ j)
		      as j = (position #\Tab line :start i)
		      collect (subseq line i j)
		      while j)
		 (declare (ignore id))
		 (list name (intern (string-upcase type) "KEYWORD"))))))

(defmethod save ((dmat xgb-matrix) file)
  (check-errors (xgb.ffi:xgd-matrix-save-binary (pointer dmat) (namestring file) 0)))

(defmethod slice ((dmat xgb-matrix) rows)
  (xgb-matrix
   (with-output-value (handle :pointer)
     (cffi:with-foreign-object (idxset :int (length rows))
       (loop for row in rows for i from 0
	  do (setf (cffi:mem-aref idxset :int i) row))
       (check-errors (xgb.ffi:xgd-matrix-slice-d-matrix (pointer dmat) idxset (length rows) handle))))))

;; TODO: xgb.ffi:xgd-matrix-slice-d-matrix-ex

(defmethod get-info-uint ((dmat xgb-matrix) field)
  (with-output-array (res :unsigned-int len)
    (check-errors (xgb.ffi:xgd-matrix-get-u-int-info (pointer dmat) field len res))))

(defmethod get-info-float ((dmat xgb-matrix) field)
  (with-output-array (res :float len)
    (check-errors (xgb.ffi:xgd-matrix-get-float-info (pointer dmat) field len res))))

(defmethod get-info ((dmat xgb-matrix) field)
  (ecase field
    (:group-ptr (get-info-uint dmat "group_ptr"))
    (:label (get-info-float dmat "label"))
    (:weight (get-info-float dmat "weight"))
    (:base-margin (get-info-float dmat "base_margin"))))

(defmethod set-info-float ((dmat xgb-matrix) field data &optional (missing 0.0))
  (declare (optimize (speed 3)))
  (cffi:with-foreign-object (in :float (length data))
    (loop for i fixnum below (length data)
       for el in data
       do (setf (cffi:mem-aref in :float i) (if el (coerce el 'single-float) missing)))
    (check-errors (xgb.ffi:xgd-matrix-set-float-info (pointer dmat) field in (length data))))
  dmat)

(defmethod set-info-uint ((dmat xgb-matrix) field data)
  (declare (optimize (speed 3)))
  (cffi:with-foreign-object (in :int (length data))
    (loop for i fixnum below (length data)
       do (setf (cffi:mem-aref in :int i) (coerce (elt data i) 'integer)))
    (check-errors (xgb.ffi:xgd-matrix-set-u-int-info (pointer dmat) field in (length data))))
  dmat)

(defmethod set-group ((dmat xgb-matrix) data)
  (cffi:with-foreign-object (in :int (length data))
    (loop for i below (length data)
       do (setf (cffi:mem-aref in :int i) (coerce (elt data i) 'integer)))
    (check-errors (xgb.ffi:xgd-matrix-set-group (pointer dmat) in (length data))))
  dmat)

(defmethod set-info ((dmat xgb-matrix) field values)
  (assert (= (length values) (nrow dmat)))
  (ecase field
    (:group (set-group dmat values))
    (:group-ptr (set-info-uint dmat "group_ptr" values))
    (:label (set-info-float dmat "label" values))
    (:weight (set-info-float dmat "weight" values))
    (:base-margin (set-info-float dmat "base_margin" values)))
  dmat)

(defmethod load-model ((booster xgb-booster) file)
  (check-errors (xgb.ffi:xg-booster-load-model (pointer booster) (namestring file))))

(defmethod save ((booster xgb-booster) file)
  (check-errors (xgb.ffi:xg-booster-save-model (pointer booster) (namestring file))))

(defmethod load-model-from-string ((booster xgb-booster) txt)
  (cffi:with-foreign-string ((buffer len) txt)
    (check-errors (xgb.ffi:xg-booster-load-model-from-buffer (pointer booster) buffer len))))

(defun booster (&rest args &key cache model-file &allow-other-keys)
  (when (not (listp cache)) (setf cache (list cache)))
  (let ((booster
	  (xgb-booster
	   (with-output-value (handle :pointer)
	     (cffi:with-foreign-object (dmats :pointer (length cache))
	       (loop for dmat in cache
		     for i from 0
		     do (setf (cffi:mem-aref dmats :pointer i) (pointer dmat)))
	       (check-errors (xgb.ffi:xg-booster-create dmats (length cache) handle)))))))
    (when model-file (load-model booster model-file))
    (setf (params booster)
	  (loop for (param value) on args by #'cddr
		unless (member param '(:cache :model-file))
		  do (xgb:set-param booster param value)
		append (list param value)))
    booster))
  
(defmethod set-param ((booster xgb-booster) name value)
  (check-errors (xgb.ffi:xg-booster-set-param (pointer booster)
					      (if (symbolp name)
						  (substitute #\_ #\- (string-downcase (symbol-name name)))
						  name)
					      (format nil "~A"
						      (if (symbolp value)
							  (substitute #\_ #\- (string-downcase (symbol-name value)))
							  value)))))

(defmethod get-num-feature ((booster xgb-booster))
  (with-output-value (out :int)
      (check-errors (xgb.ffi:xg-booster-get-num-feature (pointer booster) out))))

(defmethod get-attribute ((booster xgb-booster) name)
  (destructuring-bind (name success)
      (with-output-values ((out :string) (success :int))
	(check-errors (xgb.ffi:xg-booster-get-attr (pointer booster) name out success)))
    (when (plusp success)
      name)))

(defmethod set-attribute ((booster xgb-booster) name value)
  (check-errors (xgb.ffi:xg-booster-set-attr (pointer booster) name value)))

(defmethod attribute-names ((booster xgb-booster))
  (with-output-array (out :string len)
    (check-errors (xgb.ffi:xg-booster-get-attr-names (pointer booster) len out))))

(defmethod update-one-iter ((booster xgb-booster) (dmat xgb-matrix) iter)
  (check-errors (xgb.ffi:xg-booster-update-one-iter (pointer booster) iter (pointer dmat))))

(defmethod predict ((booster xgb-booster) (dmat xgb-matrix) &key (ntree-limit 0) training margin leaf-index features-contribution)
  ;;  option_mask bit-mask of options taken in prediction, possible values
  ;;       0:normal prediction
  ;;       1:output margin instead of transformed value
  ;;       2:output leaf index of trees instead of leaf value, note leaf index is unique per tree
  ;;       4:output feature contributions to individual predictions
  ;;  ntree_limit limit number of trees used for prediction, this is only valid for boosted trees
  ;;     when the parameter is set to 0, we will use all the trees
  (declare (optimize (speed 3)))
  (if (and features-contribution (or margin leaf-index))
      (error "features-contribution cannot be used at the same time than margin or leaf-index. 
See https://github.com/dmlc/xgboost/pull/2003"))
  (let ((option-mask (+ (if margin 1 0) (if leaf-index 2 0) (if features-contribution 4 0))))
    (cffi:with-foreign-objects ((res :pointer) (len :int))
      (xgb.ffi:xg-booster-predict (pointer booster) (pointer dmat) option-mask ntree-limit (if training 1 0) len res)
      (if (or leaf-index (> (cffi:mem-ref len :int) (nrow dmat)))
	  (loop repeat (nrow dmat)
	     with i fixnum = 0
	     collect (loop repeat (/ (cffi:mem-ref len :int) (nrow dmat))
			for value = (cffi:mem-aref (cffi:mem-ref res :pointer) :float i)
			collect (if leaf-index (round value) value)
			do (incf i)))
	  (loop for i below (cffi:mem-ref len :int) collect (cffi:mem-aref (cffi:mem-ref res :pointer) :float i))))))

(defmethod eval-one-iter ((booster xgb-booster) iter matrices names)
  (declare (optimize (speed 3)))
  (assert (= (length matrices) (length names)))
  (with-output-value (res :string)
    (cffi:with-foreign-objects ((dmats :pointer (length matrices))
				(evnames :pointer (length names)))
      (loop for i from 0 for matrix in matrices for name in names
	 do (setf (cffi:mem-aref dmats :pointer i) (pointer matrix)
		  (cffi:mem-aref evnames :pointer i) (cffi:foreign-string-alloc name)))
      (check-errors (xgb.ffi:xg-booster-eval-one-iter (pointer booster) iter dmats evnames (length matrices) res)))))

(defmethod boost-one-iter ((booster xgb-booster) (dmat xgb-matrix) grad hess)
  (assert (= (length grad) (length hess)))
  (cffi:with-foreign-objects ((%grad :float (length grad))
			      (%hess :float (length hess)))
    (loop for i from 0 for g in grad for h in hess
       do (setf (cffi:mem-aref %grad :float i) g
		(cffi:mem-aref %hess :float i) h))
    (check-errors (xgb.ffi:xg-booster-boost-one-iter (pointer booster) (pointer dmat) %grad %hess (length grad))))
  booster)

(defmethod dump-model ((booster xgb-booster) &key features stats (format :text))
  (assert (member format '(:text :json)))
  (with-output-array (res :string len)
    (if (null features)
	(check-errors (xgb.ffi:xg-booster-dump-model-ex (pointer booster) "" (if stats 1 0) (format nil "~(~a~)" format) len res))
	(cffi:with-foreign-objects ((names :pointer (length features))
				    (types :pointer (length features)))
	  (cffi:with-foreign-strings ((binary "i") (quantitative "q") (integer "int"))
	    (loop for i from 0 for (name type) in features
	       do (setf (cffi:mem-aref types :pointer i) (ecase type
							   (:i binary)
							   (:indicator binary)
							   (:q quantitative)
							   (:quantitative quantitative)
							   (:int integer)
							   (:integer integer))
			(cffi:mem-aref names :pointer i) (cffi:foreign-string-alloc name)))
	    (check-errors (xgb.ffi:xg-booster-dump-model-ex-with-features (pointer booster) (length features) names types
									  (if stats 1 0) (format nil "~(~a~)" format) len res)))
	  (loop for i below (length features)
	     do (cffi:foreign-string-free (cffi:mem-aref names :pointer i)))))))
;; not done:
;; XGBoosterDumpModel
;; XGBoosterDumpModelWithFeatures

(defmethod get-raw ((booster xgb-booster))
  (with-output-array (res :char len)
    (check-errors (xgb.ffi:xg-booster-get-model-raw (pointer booster) len res))))

(defun decode-message (msg)
  (loop for i = (1+ (position #\] msg)) then (1+ j)
     for j = (position #\Tab msg :start i)
     for entry = (subseq msg i j)
     for sc = (position #\: entry)
     when sc collect (cons (subseq entry 0 sc)
			   (read-from-string (subseq entry (1+ sc))))
     while j))

(defmethod train ((booster xgb-booster) train rounds &key objective watch-list fun-eval return-evals (iteration 0) (verbose t))
  (let ((output (make-hash-table :test #'equal)))
    (loop for iter from iteration repeat rounds
       with names = (mapcar #'second watch-list)
       with dmats = (mapcar #'first watch-list)
       do (if objective
	      (let ((pred (predict booster train)))
		(destructuring-bind (grad hess) (funcall objective pred train)
		  (boost-one-iter booster train grad hess)))
	      (update-one-iter booster train iter))
       when (and watch-list (or verbose return-evals))
       do (let ((msg (if fun-eval
			 (format nil "[~A]~3,8@T~{~A-~A:~,6F~^~3,8@T~}"
				 iter (loop for name in names for dmat in dmats
					 for (error-name value) = (funcall fun-eval (predict booster dmat) dmat)
					 append (list name error-name value)))
			 (eval-one-iter booster iter dmats names))))
	    (when verbose (format t "~&~A~&" msg))
	    (when return-evals
	      (loop for (k . v) in (decode-message msg)
		 unless (gethash k output) do (setf (gethash k output) nil)
		 do (push v (gethash k output))))))
    (loop for k in (sort (loop for k being the hash-keys of output collect k) #'string<)
       for hyphen = (position #\- k)
       for dataset = (subseq k 0 hyphen)
       for metric = (subseq k (1+ hyphen))
       collect (append (list dataset metric) (reverse (gethash k output))))))

;; TODO

;; 2.0

;; XGDMatrixCreateFromURI (CreateFromFile is deprecated)
;; XGDMatrixCreateFromCSC
;; XGDMatrixGetQuantileCut

;; 1.7

;; XGBoosterSlice

;; XGBoosterSerializeToBuffer
;; XGBoosterUnserializeFromBuffer

;; XGBoosterSaveJsonConfig
;; XGBoosterLoadJsonConfig

;; XGBoosterSaveModelToBuffer

;; XGBoosterLoadRabitCheckpoint
;; XGBoosterSaveRabitCheckpoint

;; XGBoosterBoostedRounds
;; XGBoosterFeatureScore

;; XGBoosterPredictFromCSR
;; XGBoosterPredictFromCudaArray
;; XGBoosterPredictFromCudaColumnar
;; XGBoosterPredictFromDMatrix
;; XGBoosterPredictFromDense

;; XGBRegisterLogCallback
;; XGBCallbackSetData
;; XGBCallbackDataIterNExt
;; XGDMatrixCallbackNext
;; DataIterResetCallback

;; XGDMatrixCreateFromDT
;; XGDMatrixCreateFromDataIter
;; XGDMatrixCreateFromDense
;; XGDMatrixSetDenseInfo

;; XGDMatrixSetDenseInfo
;; XGDMatrixSetInfoFromInterface

;; XGDMatrixSetStrFeatureInfo
;; XGDMatrixSetStrFeatureInfo

;; XGDMatrixCreateFromCallback
;; XGDMatrixCreateFromCudaArrayInterface
;; XGDMatrixCreateFromCudaColumnar

;; XGDMatrixNumNonMissing

;; XGDMatrixCreateFromArrowCallback
;; XGImportArrowRecordBatch

;; XGProxyDMatrixCreate
;; XGProxyDMatrixSetDataCSR
;; XGProxyDMatrixSetDataCudaColumnar
;; XGProxyDMatrixSetDataDense

;; XGQuantileDMatrixCreateFromCallback
;; XGDeviceQuantileDMatrixCreateFromCallback
;; XGDeviceQuantileDMatrixSetDataCudaArrayInterface
;; XGDeviceQuantileDMatrixSetDataCudaColumnar

;; XGCommunicatorAllreduce
;; XGCommunicatorBroadcast
;; XGCommunicatorFinalize
;; XGCommunicatorGetProcessorName
;; XGCommunicatorGetRank
;; XGCommunicatorGetWorldSize
;; XGCommunicatorInit
;; XGCommunicatorIsDistributed
;; XGCommunicatorPrint
