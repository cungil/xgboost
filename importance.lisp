(in-package :xgboost)

(defstruct node
  id
  leaf
  split
  threshold
  yes
  no
  missing
  depth
  gain
  cover)

(defun parse-threshold (x)
  (let ((less-than (position #\< x)))
    (append (list :split (if less-than
			     (subseq x 1 less-than)
			     (subseq x 1 (1- (length x)))))
	    (list :threshold (if less-than
				 (read-from-string (subseq x (1+ less-than) (1- (length x))))
				 :binary)))))

(defun parse-values (x)
  (let ((assign (position #\= x))
	(comma (position #\, x)))
    (if comma
	(append (list (intern (string-upcase (subseq x 0 assign)) "KEYWORD")
		      (read-from-string (subseq x (1+ assign) comma)))
		(parse-values (subseq x (1+ comma))))
	(list (intern (string-upcase (subseq x 0 assign)) "KEYWORD")
	      (read-from-string (subseq x (1+ assign)))))))

(defun parse-node (x)
  (let ((id (parse-integer x :junk-allowed t))
	(colon (position #\: x))
	(space (position #\Space x)))
    (if space
	(apply #'make-node :id id
	       (append (parse-threshold (subseq x (1+ colon) space))
		       (parse-values (subseq x (1+ space)))))
	(apply #'make-node :id id
	       (parse-values (subseq x (1+ colon)))))))

(defun parse-tree (x)
  (with-input-from-string (in x)
    (loop for line = (read-line in nil nil)
       while line
       collect (parse-node (string-trim '(#\Tab) line)))))

(defun print-node (tree id &optional (level 0) (on ""))
  (let ((node (find id tree :key #'node-id)))
    (if (node-split node)
	(format t "~&~VT~A : ~A ?" (* level 4) on (node-split node))
	(format t "~&~VT~A : ~6,3F" (* level 4) on (node-leaf node)))
    (when (node-yes node) (print-node tree (node-yes node) (1+ level) "Y")
    (when (node-no node) (print-node tree (node-no node) (1+ level) "N")))))

(defun print-tree (tree)
  (print-node tree (node-id (first tree))))

(defun read-json-nodes (tree)
  (cons (apply #'make-node (loop for (a . b) in tree
				 unless (eq a :children)
				   append (list (cond ((eq a :nodeid) :id)
						      ((eq a :split--condition) :threshold)
						      (t a))
						b)))
	(mapcan #'read-json-nodes (cdr (assoc :children tree)))))

(defmethod importance ((booster xgb-booster) (features list) &optional (from-json t))
  (let ((nodes (if from-json
		   (mapcan #'read-json-nodes
			   (mapcar #'cl-json:decode-json-from-string
				   (xgb:dump-model booster :stats t :features features :format :json)))
		   (mapcan #'parse-tree (xgb:dump-model booster :stats t :features features)))))
    (let ((results (loop for var in (remove-duplicates (mapcar #'node-split nodes) :test #'string=)
			 when var
			   collect (cons var (loop for node in nodes
						   when (string= (node-split node) var)
						     sum 1 into count
						     and sum (or (node-gain node) 0) into gain
						     and sum (node-cover node) into cover
						   finally (return (list gain cover count)))))))
      (sort (loop for (var gain cover count) in results
		  with total-gain = (reduce #'+ (mapcar #'second results))
		  with total-cover = (reduce #'+ (mapcar #'third results))
		  with total-count = (reduce #'+ (mapcar #'fourth results))
		  collect (list var (/ gain total-gain) (/ cover total-cover 1.0) (/ count total-count 1.0)))
	    #'> :key #'second))))
