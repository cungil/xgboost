(defpackage #:xgboost
  (:nicknames #:xgb)
  (:use #:cl)
  (:export "VERSION" "INFO" "GET-GLOBAL-CONFIG" "SET-GLOBAL-CONFIG"
	   "READ-DATA-FILE" "CREATE-MATRIX" "CREATE-MATRIX-CSR"
	   "SAVE" "SLICE" "SET-INFO" "GET-INFO" "NROW" "NCOL" "DIMS"
	   "GET-NUM-FEATURE" "READ-FEATURES" "BOOSTER" "LOAD-MODEL" "SET-PARAM" "DUMP-MODEL"
	   "UPDATE-ONE-ITER" "EVAL-ONE-ITER" "TRAIN" "PREDICT" "IMPORTANCE"))

(defpackage #:xgboost.ffi
  (:use)
  (:nicknames #:xgb.ffi))

(defpackage #:xgboost.test
  (:use #:cl #:xgboost)
  (:export "RUN"))
