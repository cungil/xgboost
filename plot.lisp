(in-package :xgboost)

(defun plot-importance (importance &optional (measure :all) (max 15))
  (assert (member measure '(:all :gain :cover :count)))
  (if (eq measure :all)
      (r:with-par (1 3)
	(loop for measure in '(:gain :cover :count) do (plot-importance importance measure max)))
      (let ((data (reverse (subseq (sort (loop for row in importance collect (cons (first row) 
										   (ecase measure
										     (:gain (second row))
										     (:cover (third row))
										     (:count (fourth row)))))
					 #'> :key #'cdr)
				   0 (min max (length importance))))))
	(r:r% "barplot" (mapcar #'cdr data) :xlab (symbol-name measure) :names.arg (mapcar #'car data) :las 2 :horiz t))))

(defun plot-importance-ggplot2 (importance &optional (measure r::*r-nilvalue*))
  (member measure '(r::*r-nilvalue* "Gain" "Weight" "Cover" "Frequency") :test #'equalp)
  (r:ensure-r "ggplot2" "data.table" "xgboost")
  (r:with-par ()
    (r:r "print" (r:r% "xgb.plot.importance" :measure measure :importance_matrix 
		       (r:r% "data.table" :|Feature| (mapcar #'first importance) :|Gain| (mapcar #'second importance)
			     :|Cover| (mapcar #'third importance) :|Frequency| (mapcar #'fourth importance))))))

(defun plot-evals (ret &optional (yrange '(0 1.5)))
  (let ((metrics (remove-duplicates (mapcar #'second ret) :test #'string=))
	(sets (remove-duplicates (mapcar #'first ret) :test #'string=)))
    (loop for metric in metrics
       do (loop for set in sets
	     for col from 1
	     for data = (cddr (find (list set metric) ret :key (lambda (x) (subseq x 0 2)) :test #'equal))
	     do (if (= 1 col)
		    (apply #'r:r "plot" data :main metric :col col :xlab "" :ylab "" :type "l"
			   :bty "n" (when yrange (list :ylim yrange)))
		    (r:r "lines" data :col col)))
	 (r:r "legend" "topright"
	      :legend sets
	      :col (loop for set in sets for i from 1 collect i)
	      :bty "n" :lwd 2))))

#|

(plot-importance-ggplot2
 (let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
	(train (xgb:read-data-file (merge-pathnames "demo/binary_classification/agaricus.txt.train" xgboost-path) t))
	(booster (xgb:booster :silent 1 :cache (list train)
			      :booster "gbtree" :objective "binary:logistic"
			      :eta 1 :gamma 1 :max-depth 3 :min-child-weight 1))
	(features (xgb:read-features (merge-pathnames "demo/binary_classification/featmap.txt" xgboost-path)))
	(num-round 2))
   (xgb:train booster train num-round)
   (importance booster features))
 "Cover")

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/regression/machine.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/regression/machine.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :booster "gbtree" :objective "reg:linear"
			     :eta 0.1 :gamma 1 :max-depth 2 :min-child-weight 1))
       (watch-list (list (list test "eval") (list train "train")))
       (features (xgb:read-features (merge-pathnames "demo/regression/featmap.txt" xgboost-path)))
       (num-round 200))
  (r:with-par (2 2)
    (plot-evals (xgb:train booster train num-round :watch-list watch-list :return-evals t))
    (plot-importance (xgb:importance booster features) :gain 10)
    (r:r "plot" (xgb:get-info train :label) (xgb:predict booster train)
	 :xlab "actual" :ylab "predicted" :main "train" :log "xy")
    (r:r "abline" :a 0 :b 1 :col "gray")
    (r:r "plot" (xgb:get-info test :label) (xgb:predict booster test)
	 :xlab "actual" :ylab "predicted" :main "test" :log "xy")
    (r:r "abline" :a 0 :b 1 :col "gray")))

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/regression/machine.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/regression/machine.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :booster "gbtree" :objective "reg:linear"
			     :eta 0.1 :gamma 1 :max-depth 2 :min-child-weight 1))
       (watch-list (list (list test "eval") (list train "train")))
       (features (xgb:read-features (merge-pathnames "demo/regression/featmap.txt" xgboost-path)))
       (num-round 200))
  (loop repeat 10 do
       (xgb:train booster train 10 :watch-list watch-list :return-evals t)
       (plot-importance (xgb:importance booster features) :gain 10)))

|#
