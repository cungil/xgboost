(in-package :xgboost)

(defclass model ()
  ((model :accessor model :initarg :model)
   (iter :accessor iter :initform 0)
   (dtrain :accessor dtrain :initarg :dtrain)
   (dtest :accessor dtest :initarg :dtest)
   (metrics :initform nil :accessor metrics)))

(defun make-model (args train &optional test)
  (make-instance 'model
		 :model (apply #'xgb:booster
			       :cache (if test (list train test) (list train))
			       :silent 1
			       args)
		 :dtrain train
		 :dtest test))

(defmethod print-object ((m model) stream)
  (print-unreadable-object (m stream)
    (format stream "train: ~A test: ~A iter: ~A" (dtrain m) (dtest m) (iter m))))

(defmethod run ((m model) num-rounds)
  (xgb:train (model m) (dtrain m) num-rounds :return-evals t
	     :iteration (iter m)
	     :watch-list (cons (list (dtrain m) "train")
			       (when (dtest m)
				 (list (list (dtest m) "eval")))))
  (incf (iter m) num-rounds))

(defmethod predict ((m model) dset &key &allow-other-keys)
  (case dset
    (:train (predict m (dtrain m)))
    (:test (predict m (dtrain m)))
    (t (xgb:predict (model m) dset))))

(defmethod logloss ((m model) dset) 
  (case dset
    (:train (logloss m (dtrain m)))
    (:test (logloss m (dtrain m)))
    (t (let ((actual (get-info dset :label))
	     (predicted (predict m dset)))
	 (/ (loop for y in actual for p in predicted
	       sum (- (+ (* y (log p)) (* (- 1 y) (log (- 1 p))))))
	    (length actual))))))
  
