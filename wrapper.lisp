(in-package :xgboost.ffi)

;; to regenerate the autowrap definitions, delete or rename the .spec files
;; requires LLVM/Clang and c2ffi https://github.com/rpav/c2ffi

;; c_api.h is copied from xgboost/include/xgboost

(autowrap:c-include '(xgboost ffi "c_api.h") :spec-path '(xgboost ffi))

(cl:let ((library #-windows (cl:or (cl:probe-file "/opt/homebrew/Cellar/xgboost/1.7.5/lib/libxgboost.dylib")
				   (cl:probe-file "/usr/local/lib/libxgboost.dylib")
				   (cl:probe-file "/usr/local/lib/libxgboost.so")
				   (cl:probe-file (cl:merge-pathnames "xgboost/lib/libxgboost.dylib" (cl:user-homedir-pathname)))
				   (cl:probe-file (cl:merge-pathnames "xgboost/lib/libxgboost.so" (cl:user-homedir-pathname)))
				   (cl:probe-file (cl:merge-pathnames "xgboost/lib/xgboost.so" (cl:user-homedir-pathname)))
				   (cl:probe-file (cl:merge-pathnames "xgboost/lib/xgboost.dll" (cl:user-homedir-pathname))))
		  #+windows (cl:or (cl:probe-file  (cl:merge-pathnames "xgboost.dll" (cl:user-homedir-pathname)))
				   (cl:probe-file  (cl:merge-pathnames "xgboost/lib/xgboost.dll" (cl:user-homedir-pathname))))))
				   
  (cl:if library
    (cl:and (cffi:load-foreign-library library)
	    (cl:format cl:t "~%~%   Foreign library ~A loaded~%~%~%" library))
    (cl:error "xgboost library not found, edit wrapper.lisp file")))
