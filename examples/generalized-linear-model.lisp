(in-package :cl-user)

;; note that gblinear is not deterministic if multiple threads are used

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) :nthread 1
			     :objective "binary:logistic" :booster "gblinear"
			     :alpha 0.0001 :lambda 1))
       (watch-list (list (list test "eval") (list train "train")))
       (num-rounds 4))
    (xgb:train booster train num-rounds :watch-list watch-list)
    (format t "error=~,6F~&"
	    (/ (loop for pred in (xgb:predict booster train)
		  for label in (xgb:get-info train :label)
		  unless (eq (> pred 0.5) (= label 1)) sum 1.0)
	       (xgb:nrow train))))

;; [0]	eval-error:0.006828	train-error:0.006602
;; [1]	eval-error:0.001241	train-error:0.002610
;; [2]	eval-error:0.000000	train-error:0.001842
;; [3]	eval-error:0.000000	train-error:0.001075
;; error=0.001075

;; THERE IS A BUG IN THE PYTHON CODE, THE LAST CALCULATION IS NOT CORRECT

#| cd ~/xgboost/demo/guide-python; cat generalized_linear_model.py
#!/usr/bin/python
import xgboost as xgb
##
#  this script demonstrate how to fit generalized linear model in xgboost
#  basically, we are using linear model, instead of tree for our boosters
##
dtrain = xgb.DMatrix('../data/agaricus.txt.train')
dtest = xgb.DMatrix('../data/agaricus.txt.test')
# change booster to gblinear, so that we are fitting a linear model
# alpha is the L1 regularizer
# lambda is the L2 regularizer
# you can also set lambda_bias which is L2 regularizer on the bias term
param = {'silent':1, 'objective':'binary:logistic', 'booster':'gblinear',
         'alpha': 0.0001, 'lambda': 1 }

# normally, you do not need to set eta (step_size)
# XGBoost uses a parallel coordinate descent algorithm (shotgun),
# there could be affection on convergence with parallelization on certain cases
# setting eta to be smaller value, e.g 0.5 can make the optimization more stable
# param['eta'] = 1

##
# the rest of settings are the same
##
watchlist  = [(dtest,'eval'), (dtrain,'train')]
num_round = 4
bst = xgb.train(param, dtrain, num_round, watchlist)
preds = bst.predict(dtest)
labels = dtest.get_label()
print ('error=%f' % ( sum(1 for i in range(len(preds)) if int(preds[i]>0.5)!=labels[i]) /float(len(preds))))
|#

#| cd ~/xgboost/demo/guide-python; python generalized_linear_model.py
[19:09:26] 6513x127 matrix with 143286 entries loaded from ../data/agaricus.txt.train
[19:09:26] 1611x127 matrix with 35442 entries loaded from ../data/agaricus.txt.test
[0]	eval-error:0.006828	train-error:0.006602
[1]	eval-error:0.001241	train-error:0.00261
[2]	eval-error:0	train-error:0.001842
[3]	eval-error:0	train-error:0.001075
error=0.000000
|#
