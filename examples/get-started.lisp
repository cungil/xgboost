(in-package :xgboost)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :objective "binary:logistic" 
			     :max-depth 2 :eta 1))
       (num-round 2))
  (loop for iter from 0 repeat num-round do (xgb:update-one-iter booster train iter))
  (let* ((preds (predict booster test))
	 (n (length preds)))
    (list (subseq preds 0 3) (subseq preds (- n 3)))))

;; ((0.28583017 0.9239239 0.28583017) (0.9239239 0.05169873 0.9239239))

;; https://xgboost.readthedocs.io/en/latest/get_started/index.html

#| PYTHON
import xgboost as xgb
# read in data
dtrain = xgb.DMatrix('demo/data/agaricus.txt.train')
## [22:55:47] 6513x127 matrix with 143286 entries loaded from demo/data/agaricus.txt.train
dtest = xgb.DMatrix('demo/data/agaricus.txt.test')
## [22:55:47] 1611x127 matrix with 35442 entries loaded from demo/data/agaricus.txt.test
# specify parameters via map
param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
num_round = 2
bst = xgb.train(param, dtrain, num_round)
# make prediction
preds = bst.predict(dtest)
preds
## array([ 0.28583017,  0.92392391,  0.28583017, ...,  0.92392391,
##         0.05169873,  0.92392391], dtype=float32)
|#

#| R
library(xgboost)
# load data
data(agaricus.train, package='xgboost')
data(agaricus.test, package='xgboost')
train <- agaricus.train
test <- agaricus.test
# fit model
bst <- xgboost(data = train$data, label = train$label, max.depth = 2, eta = 1, nround = 2,
               nthread = 2, objective = "binary:logistic")
## [1]	train-error:0.046522
## [2]	train-error:0.022263
# predict
pred <- predict(bst, test$data)
head(pred, 3)
## [1] 0.28583017 0.92392391 0.28583017
tail(pred, 3)
## [1] 0.92392391 0.05169873 0.92392391
|#
