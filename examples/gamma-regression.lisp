(in-package :cl-user)

;; cd xgboost/demo/data; Rscript gen_autoclaims.R

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (data (with-open-file (data (merge-pathnames "demo/data/autoclaims.csv" xgboost-path))
	       (loop for line = (read-line data nil nil)
		  while line collect (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")")))))
       (features (mapcar #'butlast data))
       (labels (mapcar #'first (mapcar #'last data)))
       (dmat (xgb:set-info (xgb:create-matrix (make-array (list (length features) (length (first features))) :initial-contents features))
			   :label labels))
       (train (xgb:slice dmat (loop for i from 0 below 4741 collect i)))
       (test (xgb:slice dmat (loop for i from 4741 below 6773 collect i)))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :objective "reg:gamma" :booster "gbtree"
			     :base-score 3))
       (watch-list (list (list test "eval") (list train "train")))
       (num-rounds 30))
  (xgb:train booster train num-rounds :watch-list watch-list)
  (let  ((preds (xgb:predict booster test))
	 (labels (xgb:get-info test :label)))
    (format t "test deviance=~,6F~&"
	    (* 2 (loop for pred in preds for label in labels
		    sum (+ (/ (- label pred) pred) (- (log label)) (log pred)))))))

;; [0]	eval-gamma-nloglik:474.954529	train-gamma-nloglik:452.463409
;; [1]	eval-gamma-nloglik:352.750153	train-gamma-nloglik:336.077209
;; [2]	eval-gamma-nloglik:262.296265	train-gamma-nloglik:249.933533
;; [3]	eval-gamma-nloglik:195.363602	train-gamma-nloglik:186.194000
;; [4]	eval-gamma-nloglik:145.855774	train-gamma-nloglik:139.051682
;; [5]	eval-gamma-nloglik:109.256302	train-gamma-nloglik:104.204605
;; [6]	eval-gamma-nloglik:82.219254	train-gamma-nloglik:78.465759
;; [7]	eval-gamma-nloglik:62.265785	train-gamma-nloglik:59.474022
;; [8]	eval-gamma-nloglik:47.559406	train-gamma-nloglik:45.480103
;; [9]	eval-gamma-nloglik:36.739407	train-gamma-nloglik:35.187897
;; [10]	eval-gamma-nloglik:28.797472	train-gamma-nloglik:27.636950
;; [11]	eval-gamma-nloglik:22.986506	train-gamma-nloglik:22.115425
;; [12]	eval-gamma-nloglik:18.752245	train-gamma-nloglik:18.095552
;; [13]	eval-gamma-nloglik:15.683882	train-gamma-nloglik:15.185775
;; [14]	eval-gamma-nloglik:13.462768	train-gamma-nloglik:13.095136
;; [15]	eval-gamma-nloglik:11.881174	train-gamma-nloglik:11.607486
;; [16]	eval-gamma-nloglik:10.768040	train-gamma-nloglik:10.561778
;; [17]	eval-gamma-nloglik:9.997753	train-gamma-nloglik:9.838011
;; [18]	eval-gamma-nloglik:9.472898	train-gamma-nloglik:9.345757
;; [19]	eval-gamma-nloglik:9.124675	train-gamma-nloglik:9.018530
;; [20]	eval-gamma-nloglik:8.898951	train-gamma-nloglik:8.804001
;; [21]	eval-gamma-nloglik:8.761236	train-gamma-nloglik:8.669334
;; [22]	eval-gamma-nloglik:8.676958	train-gamma-nloglik:8.583660
;; [23]	eval-gamma-nloglik:8.631366	train-gamma-nloglik:8.533513
;; [24]	eval-gamma-nloglik:8.611819	train-gamma-nloglik:8.501334
;; [25]	eval-gamma-nloglik:8.595185	train-gamma-nloglik:8.481884
;; [26]	eval-gamma-nloglik:8.592183	train-gamma-nloglik:8.469110
;; [27]	eval-gamma-nloglik:8.586963	train-gamma-nloglik:8.458343
;; [28]	eval-gamma-nloglik:8.585186	train-gamma-nloglik:8.452868
;; [29]	eval-gamma-nloglik:8.587606	train-gamma-nloglik:8.449367
;; test deviance=2282.969700

#| cd ~/xgboost/demo/guide-python; cat gamma_regression.py
#!/usr/bin/python
import xgboost as xgb
import numpy as np

#  this script demonstrates how to fit gamma regression model (with log link function)
#  in xgboost, before running the demo you need to generate the autoclaims dataset
#  by running gen_autoclaims.R located in xgboost/demo/data.

data = np.genfromtxt('../data/autoclaims.csv', delimiter=',')
dtrain = xgb.DMatrix(data[0:4741, 0:34], data[0:4741, 34])
dtest = xgb.DMatrix(data[4741:6773, 0:34], data[4741:6773, 34])

# for gamma regression, we need to set the objective to 'reg:gamma', it also suggests
# to set the base_score to a value between 1 to 5 if the number of iteration is small
param = {'silent':1, 'objective':'reg:gamma', 'booster':'gbtree', 'base_score':3}

# the rest of settings are the same
watchlist  = [(dtest,'eval'), (dtrain,'train')]
num_round = 30

# training and evaluation
bst = xgb.train(param, dtrain, num_round, watchlist)
preds = bst.predict(dtest)
labels = dtest.get_label()
print ('test deviance=%f' % (2 * np.sum((labels - preds) / preds - np.log(labels) + np.log(preds))))
|#

#| cd ~/xgboost/demo/guide-python; python gamma_regression.py
[0]	eval-gamma-nloglik:474.955	train-gamma-nloglik:452.463
[1]	eval-gamma-nloglik:352.75	train-gamma-nloglik:336.077
[2]	eval-gamma-nloglik:262.296	train-gamma-nloglik:249.934
[3]	eval-gamma-nloglik:195.364	train-gamma-nloglik:186.194
[4]	eval-gamma-nloglik:145.856	train-gamma-nloglik:139.052
[5]	eval-gamma-nloglik:109.256	train-gamma-nloglik:104.205
[6]	eval-gamma-nloglik:82.2193	train-gamma-nloglik:78.4658
[7]	eval-gamma-nloglik:62.2658	train-gamma-nloglik:59.474
[8]	eval-gamma-nloglik:47.5594	train-gamma-nloglik:45.4801
[9]	eval-gamma-nloglik:36.7394	train-gamma-nloglik:35.1879
[10]	eval-gamma-nloglik:28.7975	train-gamma-nloglik:27.6369
[11]	eval-gamma-nloglik:22.9865	train-gamma-nloglik:22.1154
[12]	eval-gamma-nloglik:18.7522	train-gamma-nloglik:18.0956
[13]	eval-gamma-nloglik:15.6839	train-gamma-nloglik:15.1858
[14]	eval-gamma-nloglik:13.4628	train-gamma-nloglik:13.0951
[15]	eval-gamma-nloglik:11.8812	train-gamma-nloglik:11.6075
[16]	eval-gamma-nloglik:10.768	train-gamma-nloglik:10.5618
[17]	eval-gamma-nloglik:9.99775	train-gamma-nloglik:9.83801
[18]	eval-gamma-nloglik:9.4729	train-gamma-nloglik:9.34576
[19]	eval-gamma-nloglik:9.12467	train-gamma-nloglik:9.01853
[20]	eval-gamma-nloglik:8.89895	train-gamma-nloglik:8.804
[21]	eval-gamma-nloglik:8.76124	train-gamma-nloglik:8.66933
[22]	eval-gamma-nloglik:8.67696	train-gamma-nloglik:8.58366
[23]	eval-gamma-nloglik:8.63137	train-gamma-nloglik:8.53351
[24]	eval-gamma-nloglik:8.61182	train-gamma-nloglik:8.50133
[25]	eval-gamma-nloglik:8.59519	train-gamma-nloglik:8.48188
[26]	eval-gamma-nloglik:8.59218	train-gamma-nloglik:8.46911
[27]	eval-gamma-nloglik:8.58696	train-gamma-nloglik:8.45834
[28]	eval-gamma-nloglik:8.58519	train-gamma-nloglik:8.45287
[29]	eval-gamma-nloglik:8.58761	train-gamma-nloglik:8.44937
test deviance=2282.972656
|#
