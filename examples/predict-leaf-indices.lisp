(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :objective "binary:logistic" 
			     :max-depth 2 :eta 1))
       (watch-list (list (list test "eval") (list train "train")))
       (num-rounds 3))
  (xgb:train booster train num-rounds :watch-list watch-list)
  (let ((using-two-trees (xgb:predict booster test :ntree-limit 2 :leaf-index t))
	(using-all-trees (xgb:predict booster test :leaf-index t)))
    (format t "~&~A~&" (list (length using-two-trees) (length (first using-two-trees))))
    (format t "~&~{~A ~}...~{ ~A~}~&" (subseq using-two-trees 0 3)
	    (subseq using-two-trees (- (length using-two-trees) 3)))
    (format t "~&~A~&" (list (length using-all-trees) (length (first using-all-trees))))))

;; [0]	eval-error:0.042831	train-error:0.046522
;; [1]	eval-error:0.021726	train-error:0.022263
;; [2]	eval-error:0.006207	train-error:0.007063
;; (1611 2)
;; (4 3) (3 3) (4 3) ... (3 3) (5 4) (3 3)
;; (1611 3)

#| cd ~/xgboost/demo/guide-python; cat predict_leaf_indices.py
#!/usr/bin/python
import numpy as np
import xgboost as xgb

### load data in do training
dtrain = xgb.DMatrix('../data/agaricus.txt.train')
dtest = xgb.DMatrix('../data/agaricus.txt.test')
param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
watchlist  = [(dtest,'eval'), (dtrain,'train')]
num_round = 3
bst = xgb.train(param, dtrain, num_round, watchlist)

print ('start testing predict the leaf indices')
### predict using first 2 tree
leafindex = bst.predict(dtest, ntree_limit=2, pred_leaf = True)
print leafindex.shape
print leafindex
### predict all trees
leafindex = bst.predict(dtest, pred_leaf = True)
print leafindex.shape
|#

#| cd ~/xgboost/demo/guide-python; python predict_leaf_indices.py
[21:26:44] 6513x127 matrix with 143286 entries loaded from ../data/agaricus.txt.train
[21:26:44] 1611x127 matrix with 35442 entries loaded from ../data/agaricus.txt.test
[0]	eval-error:0.042831	train-error:0.046522
[1]	eval-error:0.021726	train-error:0.022263
[2]	eval-error:0.006207	train-error:0.007063
start testing predict the leaf indices
(1611, 2)
[[4 3]
 [3 3]
 [4 3]
 ...,
 [3 3]
 [5 4]
 [3 3]]
(1611, 3)
|#
