(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :max-depth 2 :eta 1))
       (watch-list (list (list test "eval") (list train "train")))
       (num-rounds 2)
       (logregobj (lambda (preds dtrain)
		    (let* ((labels (xgb:get-info dtrain :label))
			   (preds (loop for x in preds collect (/ 1d0 (+ 1d0 (exp (- x))))))
			   (grad (loop for x in preds for y in labels collect (coerce (- x y) 'single-float)))
			   (hess (loop for x in preds collect (coerce (* x (- 1d0 x)) 'single-float))))
		      (list grad hess))))
       (evalerror (lambda (preds dtrain)
		    (let* ((labels (xgb:get-info dtrain :label)))
		      (list "error" (/ (loop for x in preds for y in labels
					  unless (eq (= y 1) (plusp x)) sum 1d0)
				       (xgb:nrow dtrain)))))))
  (xgb:train booster train num-rounds :watch-list watch-list :objective logregobj :fun-eval evalerror)
  (funcall evalerror (xgb:predict booster train) train))

;; [0]     eval-error:0.042831     train-error:0.046522
;; [1]     eval-error:0.021726     train-error:0.022263
;; ("error" 0.022263165975740826d0)

#| cd ~/xgboost/demo/guide-python; cat custom_objective.py
#!/usr/bin/python
import numpy as np
import xgboost as xgb
###
# advanced: customized loss function
#
print ('start running example to used customized objective function')

dtrain = xgb.DMatrix('../data/agaricus.txt.train')
dtest = xgb.DMatrix('../data/agaricus.txt.test')

# note: for customized objective function, we leave objective as default
# note: what we are getting is margin value in prediction
# you must know what you are doing
param = {'max_depth': 2, 'eta': 1, 'silent': 1}
watchlist = [(dtest, 'eval'), (dtrain, 'train')]
num_round = 2

# user define objective function, given prediction, return gradient and second order gradient
# this is log likelihood loss
def logregobj(preds, dtrain):
    labels = dtrain.get_label()
    preds = 1.0 / (1.0 + np.exp(-preds))
    grad = preds - labels
    hess = preds * (1.0-preds)
    return grad, hess

# user defined evaluation function, return a pair metric_name, result
# NOTE: when you do customized loss function, the default prediction value is margin
# this may make buildin evalution metric not function properly
# for example, we are doing logistic loss, the prediction is score before logistic transformation
# the buildin evaluation error assumes input is after logistic transformation
# Take this in mind when you use the customization, and maybe you need write customized evaluation function
def evalerror(preds, dtrain):
    labels = dtrain.get_label()
    # return a pair metric_name, result
    # since preds are margin(before logistic transformation, cutoff at 0)
    return 'error', float(sum(labels != (preds > 0.0))) / len(labels)

# training with customized objective, we can also do step by step training
# simply look at xgboost.py's implementation of train
bst = xgb.train(param, dtrain, num_round, watchlist, logregobj, evalerror)
|#

#| cd ~/xgboost/demo/guide-python; python custom_objective.py
start running example to used customized objective function
[22:38:41] 6513x127 matrix with 143286 entries loaded from ../data/agaricus.txt.train
[22:38:41] 1611x127 matrix with 35442 entries loaded from ../data/agaricus.txt.test
[0]	eval-error:0.042831	train-error:0.046522
[1]	eval-error:0.021726	train-error:0.022263
|#
