(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :objective "binary:logistic" 
			     :max-depth 2 :eta 1))
       (watch-list (list (list test "eval") (list train "train")))
       (num-rounds 3))
  (flet ((err (pred)
	   (/ (loop for pr in pred for lab in (xgb:get-info test :label)
		 unless (eq (> pr 0.5) (= lab 1)) sum 1.0)
	      (length pred))))
    (xgb:train booster train num-rounds :watch-list watch-list)
    (let ((using-one-tree (xgb:predict booster test :ntree-limit 1))
	  (using-all-trees (xgb:predict booster test)))
      (list (err using-one-tree) (err using-all-trees)))))

;; [0]	eval-error:0.042831	train-error:0.046522
;; [1]	eval-error:0.021726	train-error:0.022263
;; [2]	eval-error:0.006207	train-error:0.007063
;; (0.04283054 0.0062073246)

#| cd ~/xgboost/demo/guide-python; cat predict_first_ntree.py
#!/usr/bin/python
import numpy as np
import xgboost as xgb

### load data in do training
dtrain = xgb.DMatrix('../data/agaricus.txt.train')
dtest = xgb.DMatrix('../data/agaricus.txt.test')
param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
watchlist  = [(dtest,'eval'), (dtrain,'train')]
num_round = 3
bst = xgb.train(param, dtrain, num_round, watchlist)

print ('start testing prediction from first n trees')
### predict using first 1 tree
label = dtest.get_label()
ypred1 = bst.predict(dtest, ntree_limit=1)
# by default, we predict using all the trees
ypred2 = bst.predict(dtest)
print ('error of ypred1=%f' % (np.sum((ypred1>0.5)!=label) /float(len(label))))
print ('error of ypred2=%f' % (np.sum((ypred2>0.5)!=label) /float(len(label))))
|#

#| cd ~/xgboost/demo/guide-python; python predict_first_ntree.py
[22:44:14] 6513x127 matrix with 143286 entries loaded from ../data/agaricus.txt.train
[22:44:14] 1611x127 matrix with 35442 entries loaded from ../data/agaricus.txt.test
[0]	eval-error:0.042831	train-error:0.046522
[1]	eval-error:0.021726	train-error:0.022263
[2]	eval-error:0.006207	train-error:0.007063
start testing prediction from first n trees
error of ypred1=0.042831
error of ypred2=0.006207
|#
