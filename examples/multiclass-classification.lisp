(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (data (with-open-file (data (merge-pathnames "demo/multiclass_classification/dermatology.data" xgboost-path))
	       (loop for line = (read-line data nil nil)
		  while line collect (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")")))))
       (labels (mapcar #'1- (mapcar #'first (mapcar #'last data)))) ;; ensure labels for the n classes are in 0...(n-1)
       (features (make-array (list (length data) (1- (length (first data)))) :initial-contents (mapcar #'butlast data))))
  (loop for i from 0 below (array-dimension features 0) do (setf (aref features i 33) (if (eq (aref features i 33) '?) 1 0)))
  (let* ((dmat (xgb:set-info (xgb:create-matrix features) :label labels))
	 (ntrain (floor (* 0.7 (length labels))))
	 (train (xgb:slice dmat (loop for i from 0 below ntrain collect i)))
	 (test (xgb:slice dmat (loop for i from ntrain below (length labels) collect i)))
	 (booster (xgb:booster :silent 1 :nthread 4 :cache (list train test)
			       :objective "multi:softmax" :num-class 6
			       :max-depth 6 :eta 0.1))
	 (booster-probs (xgb:booster :silent 1 :nthread 4 :cache (list train test)
				     :objective "multi:softprob" :num-class 6
				     :max-depth 6 :eta 0.1))
	 (watch-list (list (list train "train") (list test "test")))
	 (num-rounds 5))
    (xgb:train booster train num-rounds :watch-list watch-list)
    (format t "classificaton error=~,6F~&"
	    (/ (loop for pred in (xgb:predict booster test)
		  for label in (xgb:get-info test :label)
		  unless (= pred label) sum 1.0)
	       (xgb:nrow test)))
    (xgb:train booster-probs train num-rounds :watch-list watch-list)
    (format t "classificaton error=~,6F~&"
	    (/ (loop for pred in (xgb:predict booster-probs test)
		  for max = (loop for p in pred for i from 0
			       with best = 0 and idx = nil
			       when (> p best) do (setf best p idx i)
			       finally (return idx))
		  for label in (xgb:get-info test :label)
		  unless (= max label) sum 1.0)
	       (xgb:nrow test)))))

;; [0]	train-merror:0.011719	test-merror:0.127273
;; [1]	train-merror:0.015625	test-merror:0.127273
;; [2]	train-merror:0.011719	test-merror:0.109091
;; [3]	train-merror:0.007812	test-merror:0.081818
;; [4]	train-merror:0.007812	test-merror:0.090909
;; classificaton error=0.090909
;; [0]	train-merror:0.011719	test-merror:0.127273
;; [1]	train-merror:0.015625	test-merror:0.127273
;; [2]	train-merror:0.011719	test-merror:0.109091
;; [3]	train-merror:0.007812	test-merror:0.081818
;; [4]	train-merror:0.007812	test-merror:0.090909
;; classificaton error=0.090909

#| cd ~/xgboost/demo/multiclass_classification; cat train.py
#! /usr/bin/python
import numpy as np
import xgboost as xgb

# label need to be 0 to num_class -1
data = np.loadtxt('./dermatology.data', delimiter=',',converters={33: lambda x:int(x == '?'), 34: lambda x:int(x)-1 } )
sz = data.shape

train = data[:int(sz[0] * 0.7), :]
test = data[int(sz[0] * 0.7):, :]

train_X = train[:,0:33]
train_Y = train[:, 34]

test_X = test[:,0:33]
test_Y = test[:, 34]

xg_train = xgb.DMatrix( train_X, label=train_Y)
xg_test = xgb.DMatrix(test_X, label=test_Y)
# setup parameters for xgboost
param = {}
# use softmax multi-class classification
param['objective'] = 'multi:softmax'
# scale weight of positive examples
param['eta'] = 0.1
param['max_depth'] = 6
param['silent'] = 1
param['nthread'] = 4
param['num_class'] = 6

watchlist = [ (xg_train,'train'), (xg_test, 'test') ]
num_round = 5
bst = xgb.train(param, xg_train, num_round, watchlist );
# get prediction
pred = bst.predict( xg_test );

print ('predicting, classification error=%f' % (sum( int(pred[i]) != test_Y[i] for i in range(len(test_Y))) / float(len(test_Y)) ))

# do the same thing again, but output probabilities
param['objective'] = 'multi:softprob'
bst = xgb.train(param, xg_train, num_round, watchlist );
# Note: this convention has been changed since xgboost-unity
# get prediction, this is in 1D array, need reshape to (ndata, nclass)
yprob = bst.predict( xg_test ).reshape( test_Y.shape[0], 6 )
ylabel = np.argmax(yprob, axis=1)

print ('predicting, classification error=%f' % (sum( int(ylabel[i]) != test_Y[i] for i in range(len(test_Y))) / float(len(test_Y)) ))
|#

#| cd ~/xgboost/demo/multiclass_classification; sh runexp.sh
use existing data to run multi class classification
[0]	train-merror:0.011719	test-merror:0.127273
[1]	train-merror:0.015625	test-merror:0.127273
[2]	train-merror:0.011719	test-merror:0.109091
[3]	train-merror:0.007812	test-merror:0.081818
[4]	train-merror:0.007812	test-merror:0.090909
predicting, classification error=0.090909
[0]	train-merror:0.011719	test-merror:0.127273
[1]	train-merror:0.015625	test-merror:0.127273
[2]	train-merror:0.011719	test-merror:0.109091
[3]	train-merror:0.007812	test-merror:0.081818
[4]	train-merror:0.007812	test-merror:0.090909
predicting, classification error=0.090909
|#
