(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test)
			     :objective "binary:logistic"
			     :max-depth 2 :eta 1))
       (watch-list (list (list test "eval") (list train "train"))))
    (xgb:train booster train 1 :watch-list watch-list)
    (xgb:set-info train :base-margin (xgb:predict booster train :margin t))
    (xgb:set-info test :base-margin (xgb:predict booster test :margin t))
    (xgb:train booster train 1 :watch-list watch-list))

;; [0]	eval-error:0.042831	train-error:0.046522
;; [0]	eval-error:0.021726	train-error:0.022263

#| cd ~/xgboost/demo/guide-python; cat boost_from_prediction.py
!/usr/bin/python
import numpy as np
import xgboost as xgb

dtrain = xgb.DMatrix('../data/agaricus.txt.train')
dtest = xgb.DMatrix('../data/agaricus.txt.test')
watchlist  = [(dtest,'eval'), (dtrain,'train')]
###
# advanced: start from a initial base prediction
#
print ('start running example to start from a initial prediction')
# specify parameters via map, definition are same as c++ version
param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
# train xgboost for 1 round
bst = xgb.train( param, dtrain, 1, watchlist )
# Note: we need the margin value instead of transformed prediction in set_base_margin
# do predict with output_margin=True, will always give you margin values before logistic transformation
ptrain = bst.predict(dtrain, output_margin=True)
ptest  = bst.predict(dtest, output_margin=True)
dtrain.set_base_margin(ptrain)
dtest.set_base_margin(ptest)

print ('this is result of running from initial prediction')
bst = xgb.train( param, dtrain, 1, watchlist )
|#

#| cd ~/xgboost/demo/guide-python; python boost_from_prediction.py
[22:49:07] 6513x127 matrix with 143286 entries loaded from ../data/agaricus.txt.train
[22:49:07] 1611x127 matrix with 35442 entries loaded from ../data/agaricus.txt.test
start running example to start from a initial prediction
[0]	eval-error:0.042831	train-error:0.046522
this is result of running from initial prediction
[0]	eval-error:0.021726	train-error:0.022263
|#
