(in-package :cl-user)

(let* ((xgboost-path (merge-pathnames "xgboost/" (user-homedir-pathname)))
       (train (xgb:read-data-file
	       (merge-pathnames "demo/data/agaricus.txt.train" xgboost-path) t))
       (test (xgb:read-data-file
	      (merge-pathnames "demo/data/agaricus.txt.test" xgboost-path) t))
       (booster (xgb:booster :silent 1 :cache (list train test) 
			     :objective "binary:logistic" :max-depth 2 
			     :eval-metric "logloss" :eval-metric "error"))
       (watch-list (list (list test "eval") (list train "train")))
       (num-round 2))
  (xgb:train booster train num-round :watch-list watch-list :return-evals t))

;; [0]	eval-logloss:0.480386	eval-error:0.042831	train-logloss:0.482541	train-error:0.046522
;; [1]	eval-logloss:0.357755	eval-error:0.042831	train-logloss:0.359536	train-error:0.046522
;; (("eval" "error" (0.042831 0.042831)) ("eval" "logloss" (0.480386 0.357755)) ("train" "error" (0.046522 0.046522)) ("train" "logloss" (0.482541 0.359536)))

#| cd ~/xgboost/demo/guide-python; cat evals_result.py
#
#  This script demonstrate how to access the eval metrics in xgboost
##

import xgboost as xgb
dtrain = xgb.DMatrix('../data/agaricus.txt.train', silent=True)
dtest = xgb.DMatrix('../data/agaricus.txt.test', silent=True)

param = [('max_depth', 2), ('objective', 'binary:logistic'), ('eval_metric', 'logloss'), ('eval_metric', 'error')]

num_round = 2
watchlist = [(dtest,'eval'), (dtrain,'train')]

evals_result = {}
bst = xgb.train(param, dtrain, num_round, watchlist, evals_result=evals_result)

print('Access logloss metric directly from evals_result:')
print(evals_result['eval']['logloss'])

print('')
print('Access metrics through a loop:')
for e_name, e_mtrs in evals_result.items():
    print('- {}'.format(e_name))
    for e_mtr_name, e_mtr_vals in e_mtrs.items():
        print('   - {}'.format(e_mtr_name))
        print('      - {}'.format(e_mtr_vals))

print('')
print('Access complete dictionary:')
print(evals_result)
|#

#| cd ~/xgboost/demo/guide-python; python evals_result.py
[22:59:43] src/tree/updater_prune.cc:74: tree pruning end, 1 roots, 6 extra nodes, 0 pruned nodes, max_depth=2
[0]	eval-logloss:0.480386	eval-error:0.042831	train-logloss:0.482541	train-error:0.046522
[22:59:43] src/tree/updater_prune.cc:74: tree pruning end, 1 roots, 6 extra nodes, 0 pruned nodes, max_depth=2
[1]	eval-logloss:0.357755	eval-error:0.042831	train-logloss:0.359536	train-error:0.046522
Access logloss metric directly from evals_result:
[0.480386, 0.357755]

Access metrics through a loop:
- train
   - logloss
      - [0.482541, 0.359536]
   - error
      - [0.046522, 0.046522]
- eval
   - logloss
      - [0.480386, 0.357755]
   - error
      - [0.042831, 0.042831]

Access complete dictionary:
{'train': {'logloss': [0.482541, 0.359536], 'error': [0.046522, 0.046522]}, 'eval': {'logloss': [0.480386, 0.357755], 'error': [0.042831, 0.042831]}}
|#
