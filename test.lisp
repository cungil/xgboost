(in-package :xgboost.test)

(defvar *xgboost-path* (merge-pathnames "xgboost/" (user-homedir-pathname)))

(defun almost-equal (a b &optional (epsilon 1e-5))
  (if (and (listp a) (listp b))
      (every #'identity (mapcar (lambda (a b) (almost-equal a b epsilon)) a b))
      (if (and (numberp a) (numberp b))
	  (or (and (zerop a) (zerop b))
	      (and (not (zerop a)) (not (zerop b))
		   (< (/ (abs (- a b)) (+ a b)) epsilon)))
	  (equal a b))))

(defun run ()
  (5am:run! 'xgboost-suite))

(5am:def-suite xgboost-suite)

(5am:in-suite xgboost-suite)

(5am:test version
  (5am:is (let ((version (xgb:version)))
	    (format t "~&~{~A~^.~}~&" version)
	    (equal '(2 0) (subseq (xgb:version) 0 2)))))

(5am:test info
  (5am:finishes (format t "~&~A~&" (xgb:info))))

(5am:test get-config
  (5am:finishes (format t "~&~A~&" (xgb:get-global-config))))

(5am:test set-config
  (5am:finishes (xgb:set-global-config (xgb:get-global-config))))

(5am:test get-started
  (5am:is (almost-equal '(0.28520852 0.9240534)
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :cache (list train test) :silent 1
						     :objective "binary:logistic" 
						     :max-depth 2 :eta 1 :nthread 2)))
			  (loop for iter from 0 repeat 2 do (xgb:update-one-iter booster train iter))
			  (let ((preds (xgb:predict booster test)))
			    (cons (first preds) (last preds)))))))

(5am:test predict-first-ntree
  (5am:is (almost-equal '(0.04283054 0.0062073246)
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :cache (list train test) :silent 1
						     :objective "binary:logistic" 
						     :max-depth 2 :eta 1 :nthread 2))
			       (num-rounds 3)
			       watch-list)
			  (flet ((err (pred)
				   (/ (loop for pr in pred for lab in (xgb:get-info test :label)
					 unless (eq (> pr 0.5) (= lab 1)) sum 1.0)
				      (length pred))))
			    (xgb:train booster train num-rounds :watch-list watch-list :verbose nil)
			    (let ((using-one-tree (xgb:predict booster test :ntree-limit 1))
				  (using-all-trees (xgb:predict booster test)))
			      (list (err using-one-tree) (err using-all-trees))))))))

(5am:test custom-objective
  (5am:is (almost-equal '("error" 0.022263166)
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :cache (list train test) :silent 1
						     :max-depth 2 :eta 1))
			       (watch-list (list (list test "eval") (list train "train")))
			       (num-rounds 2)
			       (logregobj (lambda (preds dtrain)
					    (let* ((labels (xgb:get-info dtrain :label))
						   (preds (loop for x in preds collect (/ 1d0 (+ 1d0 (exp (- x))))))
						   (grad (loop for x in preds for y in labels collect (coerce (- x y) 'single-float)))
						   (hess (loop for x in preds collect (coerce (* x (- 1d0 x)) 'single-float))))
					      (list grad hess))))
			       (evalerror (lambda (preds dtrain)
					    (let* ((labels (xgb:get-info dtrain :label)))
					      (list "error" (/ (loop for x in preds for y in labels
								  unless (eq (= y 1) (plusp x)) sum 1.0)
							       (xgb:nrow dtrain)))))))
			  (xgb:train booster train num-rounds :watch-list watch-list :objective logregobj :fun-eval evalerror :verbose nil)
			  (funcall evalerror (xgb:predict booster train) train)))))

(5am:test predict-leaf-indices
  (5am:is (almost-equal '(((5 4) (6 4) (5 4)) ((6 4) (4 3) (6 4)))
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :cache (list train test) :silent 1
						     :objective "binary:logistic" 
						     :max-depth 2 :eta 1))
			       (watch-list (list (list test "eval") (list train "train")))
			       (num-rounds 3))
			  (xgb:train booster train num-rounds :watch-list watch-list :verbose nil)
			  (let ((using-two-trees (xgb:predict booster test :ntree-limit 2 :leaf-index t))
				(using-all-trees (xgb:predict booster test :leaf-index t)))
			    (declare (ignorable using-all-trees))
			    (list (subseq using-two-trees 0 3)
				  (subseq using-two-trees (- (length using-two-trees) 3))))))))

(5am:test dataset-autoclaims
  (5am:is-true (or (probe-file (merge-pathnames "demo/data/autoclaims.csv" *xgboost-path*))
		   (warn "      Rscript gen_autoclaims.R    in xgboost/demo/data to download the autoclaims dataset
[you may need to install.packages('https://cran.r-project.org/src/contrib/Archive/dummies/dummies_1.5.6.tar.gz', repos=NULL)]"))))

(5am:test (gamma-regression :depends-on dataset-autoclaims)
  (5am:is (almost-equal 2303.8708
			(let* ((data (with-open-file (data (merge-pathnames "demo/data/autoclaims.csv" *xgboost-path*))
				       (loop for line = (read-line data nil nil)
					  while line
					  collect (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")")))))
			       (features (mapcar #'butlast data))
			       (labels (mapcar #'first (mapcar #'last data)))
			       (dmat (xgb:create-matrix (make-array (list (length features) (length (first features))) :initial-contents features))))
			  (xgb:set-info dmat :label labels) 
			  (let* ((train (xgb:slice dmat (loop for i from 0 below 4741 collect i)))
				 (test (xgb:slice dmat (loop for i from 4741 below 6773 collect i)))
				 (booster (xgb:booster :cache (list train test) :silent 1
						       :objective "reg:gamma" :booster "gbtree"
						       :base-score 3))
				 (watch-list (list (list test "eval") (list train "train")))
				 (num-rounds 30))
			    (xgb:train booster train num-rounds :watch-list watch-list :verbose nil)
			    (let  ((preds (xgb:predict booster test))
				   (labels (xgb:get-info test :label)))
			      (* 2 (loop for pred in preds for label in labels
				      sum (+ (/ (- label pred) pred) (- (log label)) (log pred))))))))))

(5am:test generalized-linear-model
  (5am:is (almost-equal 0.014739751
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :silent 1 :cache (list train test) :nthread 1
						     :objective "binary:logistic" :booster "gblinear"
						     :alpha 0.001 :lambda 0.01))
			       (watch-list (list (list test "eval") (list train "train")))
			       (num-rounds 10))
			  (xgb:train booster train num-rounds :watch-list watch-list :verbose nil)
			  (/ (loop for pred in (xgb:predict booster train)
				for label in (xgb:get-info train :label)
				unless (eq (> pred 0.5) (= label 1)) sum 1.0)
			     (xgb:nrow train))))))

(5am:test boost-from-prediction
  (5am:is (equal '(0.28520852 0.9240534 0.28520852 0.28520852 0.051612873)
		 (let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			(test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			(booster (xgb:booster :cache (list train test) :silent 1
					      :objective "binary:logistic"
					      :max-depth 2 :eta 1))
			(watch-list (list (list test "eval") (list train "train"))))
		   (xgb:train booster train 1 :watch-list watch-list :verbose nil)
		   (xgb:set-info train :base-margin (xgb:predict booster train :margin t))
		   (xgb:set-info test :base-margin (xgb:predict booster test :margin t))
		   (xgb:train booster train 1 :watch-list watch-list :verbose nil)
		   (subseq (xgb:predict booster test) 0 5)))))

(5am:test dataset-multiclass
  (5am:is-true (or (probe-file (merge-pathnames "demo/multiclass_classification/dermatology.data" *xgboost-path*))
		   (warn "      sh runexp.sh    in xgboost/demo/multiclass_classification to download the dermatology dataset"))))

(5am:test (multiclass-classification :depends-on dataset-multiclass)
  (5am:is (almost-equal '(0.09090909 0.09090909)
			(let* ((data (with-open-file (data (merge-pathnames "demo/multiclass_classification/dermatology.data" *xgboost-path*))
				       (loop for line = (read-line data nil nil)
					  while line collect (read-from-string (concatenate 'string "(" (substitute #\Space #\, line) ")")))))
			       (labels (mapcar #'1- (mapcar #'first (mapcar #'last data)))) ;; ensure labels for the n classes are in 0...(n-1)
			       (features (make-array (list (length data) (1- (length (first data)))) :initial-contents (mapcar #'butlast data))))
			  (loop for i from 0 below (array-dimension features 0) do (setf (aref features i 33) (if (eq (aref features i 33) '?) 1 0)))
			  (let* ((dmat (xgb:set-info (xgb:create-matrix features) :label labels))
				 (ntrain (floor (* 0.7 (length labels))))
				 (train (xgb:slice dmat (loop for i from 0 below ntrain collect i)))
				 (test (xgb:slice dmat (loop for i from ntrain below (length labels) collect i)))
				 (booster (xgb:booster :silent 1 :nthread 4 :cache (list train test)
						       :objective "multi:softmax" :num-class 6
						       :max-depth 6 :eta 0.1))
				 (booster-probs (xgb:booster :silent 1 :nthread 4 :cache (list train test)
							     :objective "multi:softprob" :num-class 6
							     :max-depth 6 :eta 0.1))
				 (watch-list (list (list train "train") (list test "test")))
				 (num-rounds 5))
			    (xgb:train booster train num-rounds :watch-list watch-list :verbose nil)
			    (xgb:train booster-probs train num-rounds :watch-list watch-list :verbose nil)
			    (list 
			     (/ (loop for pred in (xgb:predict booster test) for label in (xgb:get-info test :label)
				   unless (= pred label) sum 1.0)
				(xgb:nrow test))
			     (/ (loop for pred in (xgb:predict booster-probs test)
				   for max = (loop with best = 0 and idx = nil
						for p in pred for i from 0
						when (> p best) do (setf best p idx i)
						finally (return idx))
				   for label in (xgb:get-info test :label)
				   unless (= max label) sum 1.0)
				(xgb:nrow test))))))))

(5am:test evals-result
  (5am:is (almost-equal '(("eval" "error" 0.04283054 0.04283054) ("eval" "logloss" 0.47976637 0.35739666)
			  ("train" "error" 0.04652234 0.04652234) ("train" "logloss" 0.48195833 0.35919428))
			(let* ((train (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.train?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (test (xgb:read-data-file (merge-pathnames "demo/data/agaricus.txt.test?format=libsvm" *xgboost-path*))) ;; verbose crashes
			       (booster (xgb:booster :silent 1 :cache (list train test) 
						     :objective "binary:logistic" :max-depth 2 
						     :eval-metric "logloss" :eval-metric "error"))
			       (watch-list (list (list test "eval") (list train "train")))
			       (num-round 2))
			  (xgb:train booster train num-round :watch-list watch-list :return-evals t :verbose nil))
			1e-4)))
