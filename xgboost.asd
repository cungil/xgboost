(in-package #:asdf)

(defsystem #:xgboost
    :name "xgboost"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :description "Common Lisp interface to https://github.com/dmlc/xgboost"
    :in-order-to ((test-op (test-op "xgboost/test")))
    :depends-on (:trivial-garbage :cffi :cl-autowrap :cl-json)
    :serial t
    :components ((:module #:ffi :pathname "ffi" :components ((:static-file "c_api.h")))
		 (:file "package")
		 (:file "wrapper")
		 (:file "xgboost")
		 (:file "importance")
		 (:file "high-level")))

(defsystem #:xgboost/test
    :name "xgboost test suite"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :depends-on (:xgboost :fiveam)
    :components ((:file "test"))
    :perform (asdf:test-op (o s) (uiop:symbol-call :xgboost.test '#:run)))

(defsystem #:xgboost.plot
    :name "xgboost plotting"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "Apache License, Version 2.0"
    :depends-on (:xgboost :rcl)
    :components ((:file "plot")))
